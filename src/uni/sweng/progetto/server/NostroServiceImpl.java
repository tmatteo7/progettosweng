package uni.sweng.progetto.server;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.dev.protobuf.Descriptors.MethodDescriptor;
import com.google.gwt.dev.protobuf.Descriptors.ServiceDescriptor;
import com.google.gwt.dev.protobuf.Message;
import com.google.gwt.dev.protobuf.RpcCallback;
import com.google.gwt.dev.protobuf.RpcController;
import com.google.gwt.dev.protobuf.Service;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import uni.sweng.progetto.client.NostroService;
import uni.sweng.progetto.shared.Elezione;

public class NostroServiceImpl extends RemoteServiceServlet implements NostroService {
	
	//metodo relativo alla nomina dei funzionari
	public String nominaFunzionario(String username) throws IllegalArgumentException{
		DB db = getDB();
		System.out.println("\n \n \n ARRIVATO " + username + " \n \n \n ");
		Map<Integer, Cittadino> funz = db.getTreeMap("Funzionari");
		if (!funz.isEmpty()) {
			Set<Integer> funzKeys = funz.keySet();
			for (Integer key : funzKeys) {
				if (funz.get(key).getUsername().equals(username)) {
					throw new IllegalArgumentException(username + " è già tra i funzionari");
				}
			}
		}
		Map<Integer, Cittadino> cit = db.getTreeMap("Cittadini");
		Set<Integer> keys = cit.keySet();
		for (Integer k:keys) {
			if (cit.get(k).getUsername().equals(username)) {
				funz.put(cit.get(k).hashCode(), cit.get(k));
				db.commit();
				return username + " inserito correttamente";
			}
		}
		throw new IllegalArgumentException(username + " non è un cittadino");
	}
	
	
	//metodo relativo alla registrazione di un utente
	public String registrazione(String nome, String cognome, String username, String password, String telefono,
			String email, String codiceFiscale, String indirizzoDomicilio, String idDocumentoIdentita) {
		DB db = getDB();
		Map<Integer, Cittadino> cit = db.getTreeMap("Cittadini");
		Cittadino c = new Cittadino(nome, cognome, username, password, telefono, email, codiceFiscale, indirizzoDomicilio, idDocumentoIdentita);
		Set<Integer> keys = cit.keySet();
		for (Integer k : keys) {
			if (cit.get(k).getUsername().equals(username))
				throw new IllegalArgumentException("Username già presente");
		}
		if (cit.containsValue(c)) {
			throw new IllegalArgumentException("Cittadino già inserito");
		}
		else {
			cit.put(c.hashCode(), c);
			db.commit();
			return  c.getUsername() + "Inserito correttamente";
		}
			
	}
	
	//metodo relativo al login del cittadino	
	public void loginCittadino (String username, String password) throws IllegalArgumentException{
		if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {
			return;
		}
		DB db = getDB();
		Map<Integer, Cittadino> citt = db.getTreeMap("Cittadini");
		Set<Integer> keys = citt.keySet();
		for (Integer k : keys) {
			if (citt.get(k).getUsername().equals(username) && citt.get(k).getPassword().equals(password)) {
				return;
			}
		}
		throw new IllegalArgumentException("Credenziali errate!");	
	}
	
	//metodo relativo al login dell'amministratore	
	public void loginAdmin (String username, String password) throws IllegalArgumentException{
		if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {
			return;
		}
		DB db = getDB();
		Map<Integer, Admin> funz = db.getTreeMap("Admin");
		Set<Integer> keys = funz.keySet();
		for (Integer k : keys) {
			if (funz.get(k).getUsername().equals(username) && funz.get(k).getPassword().equals(password)) {
				return;
			}
		}
		throw new IllegalArgumentException("Credenziali errate! \\n Forse non sei un admin?");
	}
	
	//metodo relativo al login del funzioario	
	public void loginFunzionario (String username, String password) throws IllegalArgumentException{
		if(username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")) {
			return;
		}
		DB db = getDB();
		Map<Integer, Cittadino> funz = db.getTreeMap("Funzionari");
		Set<Integer> keys = funz.keySet();
		for (Integer k : keys) {
			if (funz.get(k).getUsername().equals(username) && funz.get(k).getPassword().equals(password)) {
				return;
			}
		}
		throw new IllegalArgumentException("Credenziali errate! \n Forse non sei un funzionario comunale?");	
	}	
	
	
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}
	
	@Override
	public void creaElezioni(Elezione e) {
		DB db = getDB();
		Map<Integer, Elezione> elezioni = db.getTreeMap("Elezioni");
		elezioni.put(e.hashCode(),e);
		db.commit();
	}


	@Override
	public ArrayList<Elezione> mostraElezioni() {

		DB db= getDB();
		ArrayList<Elezione> ret = new ArrayList<Elezione>();
		Map<Integer, Elezione> elezioni = db.getTreeMap("Elezioni");
		Set<Integer> keys = elezioni.keySet();
		for (Integer k : keys) {
			String end = null;
		}
		return ret;
	}

}
