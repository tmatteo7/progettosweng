package uni.sweng.progetto.server;

import java.util.Date;

/**Un Funzionario Comunale viene scelto tra la lista dei Cittadini del sistema.*/
public class FunzionarioComunale extends Cittadino{
	private String username;
	
	public FunzionarioComunale() {
	}
	
	public FunzionarioComunale(String username) {
		this.username = username;
	}
	
	public FunzionarioComunale(String nome, String cognome) {
		super(nome,cognome);
	}
	
	public FunzionarioComunale(Cittadino c) {
		username = c.getUsername();
	}	

}
