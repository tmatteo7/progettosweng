package uni.sweng.progetto.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Election implements EntryPoint{

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	private final NostroServiceAsync service = GWT.create(NostroService.class);

	public void onModuleLoad() {
		
		RootPanel.get().clear(); //per quando ci si ritorna
		
		final Button accediButton = new Button("Accedi");
		accediButton.setStyleName("buttonHome");
		final Button registratiButton = new Button("Registrati");
		registratiButton.setStyleName("buttonHome");
		final Button risultatiButton = new Button("Visualizza Risultati! ");
		risultatiButton.setStyleName("buttonHome");
		
		HorizontalPanel homePanel = new HorizontalPanel();
		homePanel.add(risultatiButton);
		homePanel.add(accediButton);
		homePanel.add(registratiButton);
		
		RootPanel.get().add(homePanel);
		RootPanel.get().setWidgetPosition(homePanel, 550, 300);

		
		registratiButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mostraRegistrazioneBoard();		
				
			}
			
		});
		
		accediButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				mostraAccediBoard();
			}
		});
		
	}	
	
	
	/**
	 * Schermata che sar� visibile dopo aver premuto il tasto "Registrarsi".
	 * Saranno richiesti nome e cognome.
	 */
	private void mostraRegistrazioneBoard() {
		SchermataRegistrazione rb = new SchermataRegistrazione(RootPanel.get(), service, this);
		rb.show();
	}
	
	private void mostraSchermataAmministratore() {
		SchermataAmministratore sa = new SchermataAmministratore(RootPanel.get(), service, this);
		sa.show();
	}
	
	private void mostraSchermataFunzionario() {
		SchermataFunzionario sf = new SchermataFunzionario(RootPanel.get(), service, this);
		sf.show();
	}
	
	private void mostraSchermataCittadino(String nomeut) {
		SchermataCittadino sc = new SchermataCittadino(RootPanel.get(), service, this, nomeut);
		sc.show();
	}
	
	
	private void mostraAccediBoard() {
		//Inserimento credenziali
		RootPanel.get().clear();
		final HorizontalPanel usernamePanel = new HorizontalPanel();
		final TextBox usernameTextBox = new TextBox();
		usernameTextBox.getElement().setPropertyString("placeholder", "Username");
		usernameTextBox.setStyleName("textBox");
		usernamePanel.add(usernameTextBox);
		
		//Inserimento password
		final HorizontalPanel passwordPanel = new HorizontalPanel();
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.getElement().setPropertyString("placeholder", "*******");
		passwordTextBox.setStyleName("textBox");
		passwordPanel.add(passwordTextBox);
		
		final VerticalPanel credenzialiPanel = new VerticalPanel();
		credenzialiPanel.add(usernamePanel);
		credenzialiPanel.add(passwordPanel);
		
		RootPanel.get().add(credenzialiPanel);
		RootPanel.get().setWidgetPosition(credenzialiPanel, 650, 300);
		
		//Selezione tipo di accesso
		final HorizontalPanel profiliPanel = new HorizontalPanel();
		final Button cittadinoButton = new Button("Accedi come Cittadino");
		cittadinoButton.setStyleName("buttonLog");
		final Button funzionarioButton = new Button("Accedi come Funzionario Comunale");
		funzionarioButton.setStyleName("buttonLog");
		final Button adminButton = new Button("Accedi come Admin");
		adminButton.setStyleName("buttonLog");
		
		final Button indietroButton = new Button("Indietro");
		indietroButton.setStyleName("buttonEsci");
		indietroButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				onModuleLoad();		
			}		
		});
		
		profiliPanel.add(cittadinoButton);
		profiliPanel.add(funzionarioButton);
		profiliPanel.add(adminButton);
		profiliPanel.add(indietroButton);
		
		RootPanel.get().add(profiliPanel);
		RootPanel.get().setWidgetPosition(profiliPanel, 530, 450);
		
		cittadinoButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String username = usernameTextBox.getText();
				final String password = passwordTextBox.getText();
				
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert( caught.toString());
					}
					@Override
					public void onSuccess(Void result) {
						mostraSchermataCittadino(username);
					}	
				};
				service.loginCittadino(username, password, callback);	
			}
			
		});
		
		funzionarioButton.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String username = usernameTextBox.getText();
				String password = passwordTextBox.getText();
				// Set up the callback object.
				 AsyncCallback<Void> callback = new AsyncCallback<Void>() {	
					 @Override
						public void onFailure(Throwable caught) {
							Window.alert( caught.toString() );		
						}
					@Override
						public void onSuccess(Void r) {
							mostraSchermataFunzionario();
						} 
				 };
				 service.loginFunzionario(username, password, callback);
			}
		});
		
		adminButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String username = usernameTextBox.getText();
				String password = passwordTextBox.getText();
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert( caught.toString() );		
					}
					@Override
					public void onSuccess(Void r) {
						mostraSchermataAmministratore();
					}
				};
				service.loginAdmin(username, password, callback);
			}
			
		});
		
		
	}

}
