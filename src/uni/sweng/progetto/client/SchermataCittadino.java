package uni.sweng.progetto.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import uni.sweng.progetto.shared.Elezione;


public class SchermataCittadino {

	private RootPanel RootPanel;
	private NostroServiceAsync service;
	private final Election election;
	private final String username;
	
	private Elezione scelta = null;

	public SchermataCittadino(RootPanel rp, NostroServiceAsync service, Election election, String username) {
		RootPanel = rp;
		this.service = service;
		this.election = election;
		this.username = username;
	}

	public void show() {

		
		RootPanel.clear();
		//TODO: AlreadyVoteException
		AsyncCallback<ArrayList<Elezione>> callback = new AsyncCallback<ArrayList<Elezione>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getMessage());
				
			}
			@Override
			public void onSuccess(ArrayList<Elezione> result) {
				if (result.isEmpty()) {
					Window.alert("Non ci sono elezioni a cui partecipare al momento!");
				}
				else {
					creaTabellaElezioni(result, username); //Viene aggiunta una tabella contenente tutte le elezioni a cui il cittadino pu� votare.
				}
			}
		};
		service.mostraElezioni(callback);
		
		Button tornaLoginButton = new Button("Esci");
		tornaLoginButton.setStyleName("buttonEsci");
		
		
		tornaLoginButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				election.onModuleLoad();
			};
				
			
		});		
		RootPanel.add(tornaLoginButton);
		RootPanel.setWidgetPosition(tornaLoginButton, 700, 200);
		
		
	
	}
	
	
	private void creaTabellaElezioni(ArrayList<Elezione> e, String uname) {
		
		final String username = uname;
		final VerticalPanel tabellaPanel = new VerticalPanel();
		final Label tabellaLabel = new Label("Seleziona una elezione tra quelle indicate sotto");
		tabellaLabel.setStyleName("labelRegistrazione");
		tabellaLabel.getElement().getStyle().setProperty("fontFamily", "Calibri");
		tabellaLabel.getElement().getStyle().setFontSize(1.5, Unit.EM);
		final CellTable<Elezione> tabella = new CellTable<Elezione>();
		final HorizontalPanel sceltaPanel = new HorizontalPanel();
		
        Button votoButton = new Button("Vota!");
        votoButton.setStyleName("buttonCittadino");
        Button presentaListaButton = new Button("Presenta una lista!");
        presentaListaButton.setStyleName("buttonCittadino");
        
        sceltaPanel.add(votoButton);
        sceltaPanel.add(presentaListaButton);
        
        tabellaPanel.add(sceltaPanel);
        RootPanel.add(tabellaPanel);
        //RootPanel.get().setWidgetPosition(tabellaPanel, 600, 220);
        
		//Vengono create le due colonne della tabella: Data inizio e Data fine.
		//Data di inizio
	    TextColumn<Elezione> colonnaInizio = new TextColumn<Elezione>() {
			@Override
			public String getValue(Elezione elezione) {
				return elezione.getStringStart();
			}
	    };
	    tabella.addColumn(colonnaInizio, "Data di inizio");
	    //Data di fine
	    TextColumn<Elezione> colonnaFine = new TextColumn<Elezione>() {
			@Override
			public String getValue(Elezione elezione) {
				return elezione.getStringEnd();
			}
	    };
	    tabella.addColumn(colonnaFine, "Data di fine");
	    
	    //Popolamento della tabella.
	    //1 - Settare il numero totale di righe, non � strettamente necessario ma aiuta il calcolo della paginazione.
	    //table.setRowCount(e.size(), true);
	    //2 - Inserire i dati all'interno della tabella con setRowData().
	    
	    tabella.setRowData(0, e);
	    if (tabella.getRowCount() == 0) {
	    	Window.alert("Non ci sono elezioni a cui partecipare in questo momento");
	    } else {
	    //aggiunta alla root
	    tabellaPanel.add(tabellaLabel);
	    tabellaPanel.add(tabella);
	    }
        
        
	    // Add a selection model to handle user selection.
	    final SingleSelectionModel<Elezione> selezione = new SingleSelectionModel<Elezione>();
	    tabella.setSelectionModel(selezione);
	    
	    selezione.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
	      public void onSelectionChange(SelectionChangeEvent event) {
	        final Elezione selected = selezione.getSelectedObject();
	        if (selected != null) {
	        	Window.alert("Hai scelto: " + selected.getStringStart() + " / " + selected.getStringEnd()+" e il suo id è = "+selected.getIDElection());
	        	scelta = selected;	
	        }
	      }
	    });
	    

	    //fine
        RootPanel.get().add(tabellaPanel);
	    RootPanel.get().setWidgetPosition(tabellaPanel, 600, 270);
	    tabellaPanel.setSpacing(30);
	    
	}
}
