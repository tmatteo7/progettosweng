package uni.sweng.progetto.client;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class SchermataAmministratore {
	
	private RootPanel RootPanel;
	private NostroServiceAsync service;
	private final Election election;
	
	public SchermataAmministratore(RootPanel rp, NostroServiceAsync rsa, Election election) {
		RootPanel = rp;
		service = rsa;
		this.election = election;
	}
	
	public void show() {
		RootPanel.clear();
		final HorizontalPanel nominaPannello = new HorizontalPanel();
		final MultiWordSuggestOracle oracle = new MultiWordSuggestOracle();		
		AsyncCallback<List<String>> callback = new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(List<String> result) {
				// TODO Auto-generated method stub
				
			}

		};
		
		final TextBox usernameCittadino = new TextBox();
		usernameCittadino.getElement().setPropertyString("placeholder", "Username");
		
		final Button nominaFunzionario = new Button("Nomina il cittadino funzionario");
		final Button esci = new Button("Esci");
		
		nominaPannello.add(usernameCittadino);
		nominaPannello.add(nominaFunzionario);
		
		RootPanel.add(nominaPannello);
		RootPanel.add(esci);
		
		RootPanel.get().setWidgetPosition(nominaPannello, 600, 300);
		RootPanel.get().setWidgetPosition(esci, 600, 370);
		
		nominaFunzionario.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				AsyncCallback<String> callback = new AsyncCallback<String>(){

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						Window.alert(result);
						usernameCittadino.setText("");
					}
				};
				service.nominaFunzionario(usernameCittadino.getText(), callback);
			}
		});
		
		esci.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				election.onModuleLoad();
			};


		});
	}
}
