package uni.sweng.progetto.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;

import uni.sweng.progetto.shared.Elezione;


public interface NostroServiceAsync {
	
	void registrazione(String nome, String cognome, String username, String password, 
			String telefono, String email, String codiceFiscale, String indirizzoDomicilio, 
			String idDocumentoIdentita, AsyncCallback<String> callback);

	void nominaFunzionario(String username, AsyncCallback<String> callback);
	
	void loginCittadino(String username, String password, AsyncCallback<Void> callback);
	void loginFunzionario(String username, String password, AsyncCallback<Void> callback);	
	void loginAdmin(String username, String password, AsyncCallback<Void> callback);

	void creaElezioni(Elezione e, AsyncCallback<Void> callback);
	void mostraElezioni(AsyncCallback<ArrayList<Elezione>> callback);


}
