package uni.sweng.progetto.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import uni.sweng.progetto.client.Election;
import uni.sweng.progetto.shared.Elezione;

public class SchermataFunzionario {

	private RootPanel RootPanel;
	private NostroServiceAsync service;
	private final Election election;
	private Date dataInizioSend;
	private Date dataFineSend;
	
	
	public SchermataFunzionario(RootPanel root , NostroServiceAsync service,Election election){
		this.RootPanel = root;
		this.service = service;
		this.election = election;
	}

	public void show(){

		RootPanel.clear();
		final VerticalPanel tabellaPanel = new VerticalPanel();
		final HorizontalPanel bottoniListePanel = new HorizontalPanel();
		final HorizontalPanel indiciElezionePanel = new HorizontalPanel();
		final Button esci = new Button("Esci");
		
		RootPanel.add(tabellaPanel); 
		RootPanel.add(indiciElezionePanel);
		RootPanel.add(esci);
		
		RootPanel.setWidgetPosition(esci, 400, 220);
		RootPanel.setWidgetPosition(tabellaPanel, 850, 300);
		
		schermataIndiciElezione();
		
		esci.addClickHandler(new ClickHandler() {	
			@Override
			public void onClick(ClickEvent event) {
				election.onModuleLoad();
			};	
	});	
	}
	/**
	 * Schermata che presenta due calendari tramite i quali l'utente pu� scegliere i giorni di inizioe fine dell'elezione.
	 * Un bottone "Indici elezione" invia la richiesta al server.
	 */
	private void schermataIndiciElezione() {
		//creo i pannelli
		final HorizontalPanel hPanelIndizione = new HorizontalPanel();
		final VerticalPanel vPanelInizio = new VerticalPanel();
		final VerticalPanel vPanelFine = new VerticalPanel();
				
		//Creo il popup per la scelta grafica della data di inizio
		final TextBox dataInizioTB = new TextBox();
		final Label dataInizio = new Label("Data di inizio:");
		//Creo il popup per la scelta grafica della data di fine
		final TextBox dataFineTB = new TextBox();
		final Label dataFine = new Label("Data di fine:");		
		
		vPanelInizio.add(dataInizio);
		vPanelInizio.add(dataInizioTB);
		vPanelFine.add(dataFine);
		vPanelFine.add(dataFineTB);
	
		hPanelIndizione.add(vPanelInizio);
		hPanelIndizione.add(vPanelFine);
		RootPanel.add(hPanelIndizione);
		RootPanel.setWidgetPosition(hPanelIndizione, 0, 0);
		
		//Creo il bottone per l'indizione di un elezione.
		final Button indiciElezioneButton = new Button("Indici elezione");
		RootPanel.add(indiciElezioneButton);
		RootPanel.setWidgetPosition(indiciElezioneButton, 390, 570);	
		
		
		//Creo il popup per la scelta grafica dell'ora di inizio
		final TextBox oraInizioTB = new TextBox();
		final Label oraInizioL = new Label("Ora di inizio:");
		oraInizioTB.setValue("00:00");
		vPanelInizio.add(oraInizioL);
		vPanelInizio.add(oraInizioTB);
		
		
		//Creo il popup per la scelta grafica dell'ora di fine
		final TextBox oraFineTB = new TextBox();
		final Label oraFineL = new Label("Ora di fine:");
		oraFineTB.setValue("00:00");

		vPanelFine.add(oraFineTB);
		vPanelFine.add(oraFineL);

		indiciElezioneButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String inizio = dataInizioTB.getValue();
				String fine = dataInizioTB.getValue();


				// Set up the callback object.
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString() );

					}
					@Override
					public void onSuccess(Void result) {
						Window.alert("Elezione inserita");
					} 	
				};
				service.creaElezioni(new Elezione(inizio, fine), callback);
			}
		});
	}
}