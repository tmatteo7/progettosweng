package uni.sweng.progetto.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;



public class SchermataRegistrazione{
	private RootPanel RootPanel;
	private NostroServiceAsync service;
	private final Election election;
	
	public SchermataRegistrazione(RootPanel rp, NostroServiceAsync service, Election elect){
		this.RootPanel = rp;
		this.service = service;
		this.election = elect;

	}

	public void show(){

		RootPanel.get().clear();
		
		final VerticalPanel pane = new VerticalPanel();
		RootPanel.get().add(pane);
		RootPanel.get().setWidgetPosition(pane, 600, 250);
		
		final Label datiLabel = new Label("DATI DELL'UTENTE");
		datiLabel.setStyleName("labelRegistrazione");
		datiLabel.getElement().getStyle().setProperty("fontFamily", "Calibri");
		datiLabel.getElement().getStyle().setFontSize(2, Unit.EM);
		RootPanel.get().add(datiLabel);
		RootPanel.get().setWidgetPosition(datiLabel, 610, 210);
		
		final HorizontalPanel nomePane = new HorizontalPanel();
		final TextBox nomeTextBox = new TextBox();
		nomeTextBox.getElement().setPropertyString("placeholder", "Nome");
		nomeTextBox.setStyleName("textBoxRegistrazione");
		nomePane.add(nomeTextBox);
		pane.add(nomePane);
		
		final HorizontalPanel cognomePane = new HorizontalPanel();
		final TextBox cognomeTextBox = new TextBox();
		cognomeTextBox.getElement().setPropertyString("placeholder", "Cognome");
		cognomeTextBox.setStyleName("textBoxRegistrazione");
		cognomePane.add(cognomeTextBox);
		pane.add(cognomePane);
		
		final HorizontalPanel telefonoPane = new HorizontalPanel();
		final TextBox telefonoTextBox = new TextBox();
		telefonoTextBox.getElement().setPropertyString("placeholder", "Telefono");
		telefonoTextBox.setStyleName("textBoxRegistrazione");
		telefonoTextBox.addValueChangeHandler(new ValueChangeHandler<String>() {

	        @Override
	        public void onValueChange(ValueChangeEvent<String> event) {
	            if (!event.getValue().matches("\\d+")) {
	            	Window.alert("Inserire solo i caratteri 1234567890" );
	            	telefonoTextBox.setText("");
	            	telefonoTextBox.setFocus(true);
	            }
	        }

	    });		
		telefonoPane.add(telefonoTextBox);
		pane.add(telefonoPane);
		
		final HorizontalPanel mailPane = new HorizontalPanel();
		final TextBox mailTextBox = new TextBox();
		mailTextBox.getElement().setPropertyString("placeholder", "E-mail");
		mailTextBox.setStyleName("textBoxRegistrazione");
		mailPane.add(mailTextBox);
		pane.add(mailPane);
		
		final HorizontalPanel cfPane = new HorizontalPanel();
		final TextBox cfTextBox = new TextBox();
		cfTextBox.getElement().setPropertyString("placeholder", "Codice Fiscale");
		cfTextBox.setStyleName("textBoxRegistrazione");
		cfPane.add(cfTextBox);
		pane.add(cfPane);
		
		final HorizontalPanel indirizzoPane = new HorizontalPanel();
		final TextBox indirizzoTextBox = new TextBox();
		indirizzoTextBox.getElement().setPropertyString("placeholder", "Indirizzo");
		indirizzoTextBox.setStyleName("textBoxRegistrazione");
		indirizzoPane.add(indirizzoTextBox);
		pane.add(indirizzoPane);
		
		final HorizontalPanel docPane = new HorizontalPanel();
		
		final Label tipoDocLabel = new Label("DOCUMENTO D'IDENTITA'");
		tipoDocLabel.setStyleName("labelRegistrazione");
		tipoDocLabel.getElement().getStyle().setProperty("fontFamily", "Calibri");
		tipoDocLabel.getElement().getStyle().setFontSize(2, Unit.EM);
		RootPanel.get().add(tipoDocLabel);
		RootPanel.get().setWidgetPosition(tipoDocLabel, 580, 640);
		
		final ListBox tipoDocBox = new ListBox();
		tipoDocBox.setStyleName("textBoxRegistrazione");
		tipoDocBox.addItem("Carta d'identità", "carta");
	    tipoDocBox.addItem("Passaporto", "passaporto");
		final TextBox numDocTextBox = new TextBox();
		numDocTextBox.getElement().setPropertyString("placeholder", "Numero");
		numDocTextBox.setStyleName("textBoxRegistrazione");
		final TextBox rilascioTextBox = new TextBox();
		rilascioTextBox.getElement().setPropertyString("placeholder", "Ente che lo ha rilasciato");
		rilascioTextBox.setStyleName("textBoxRegistrazione");
		
		final HorizontalPanel docPickerPane = new HorizontalPanel();
		final VerticalPanel dataPanel1 = new VerticalPanel();
		final VerticalPanel dataPanel2 = new VerticalPanel();
		final Label dataRilascioLabel = new Label("Data di rilascio:");
		final DatePicker dataRilascio = new DatePicker();
		dataPanel1.add(dataRilascioLabel);dataPanel1.add(dataRilascio);
		final Label dataScadenzaLabel = new Label("Data di scadenza:");
		final DatePicker dataScadenza = new DatePicker();
		dataPanel2.add(dataScadenzaLabel);dataPanel2.add(dataScadenza);
		
		dataPanel1.setSpacing(10);
		dataPanel2.setSpacing(10);
		
		dataScadenza.addValueChangeHandler(new ValueChangeHandler<Date>() {
			
	        @Override
	        public void onValueChange(ValueChangeEvent<Date> event) {
	        	if (event.getValue().before(dataRilascio.getValue())){
	        		Window.alert("Controlla meglio le date");
	        	}
	        }
			
		});
		
		docPane.add(tipoDocBox); docPane.add(numDocTextBox);docPane.add(rilascioTextBox);
		docPane.setSpacing(20);
		
		docPickerPane.add(dataPanel1);
		docPickerPane.add(dataPanel2);
		docPickerPane.setSpacing(40);
		
		RootPanel.get().add(docPane);
		RootPanel.get().setWidgetPosition(docPane, 410, 660);
		RootPanel.get().add(docPickerPane);
		RootPanel.get().setWidgetPosition(docPickerPane, 430, 720);

		final VerticalPanel credPane = new VerticalPanel();
		final HorizontalPanel usernamePane = new HorizontalPanel();
		final TextBox usernameTextBox = new TextBox();
		usernameTextBox.getElement().setPropertyString("placeholder", "Username");
		usernameTextBox.setStyleName("textBoxRegistrazione");
		usernamePane.add(usernameTextBox);
		//pane.add(usernamePane);
		credPane.add(usernamePane);
		
		final HorizontalPanel passwordPane = new HorizontalPanel();
		final PasswordTextBox passwordTextBox = new PasswordTextBox();
		passwordTextBox.getElement().setPropertyString("placeholder", "**********");
		passwordTextBox.setStyleName("textBoxRegistrazione");
		passwordPane.add(passwordTextBox);
		//pane.add(passwordPane);
		credPane.add(passwordPane);
		RootPanel.get().add(credPane);
		RootPanel.get().setWidgetPosition(credPane, 600, 1000);
		
		final Button registrazioneButton = new Button("Registrati!");
		registrazioneButton.setStyleName("buttonRegistrazione");
		registrazioneButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (nomeTextBox.getValue() == null || cognomeTextBox.getValue() == null 
						|| telefonoTextBox.getValue() == null || mailTextBox.getValue() == null 
						|| cfTextBox.getValue() == null || indirizzoTextBox.getValue() == null 
						|| numDocTextBox.getValue() == null || rilascioTextBox.getValue() == null 
						|| dataRilascio.getValue() == null || dataScadenza.getValue() == null 
						|| usernameTextBox.getValue() == null || passwordTextBox.getValue() == null) {
					Window.alert("Tutti i campi sono obbligatori");
				}
				
				else {
					 AsyncCallback<String> callback = new AsyncCallback<String>() {
							@Override
							public void onFailure(Throwable caught) {
								Window.alert( caught.getMessage());
								
							}
							@Override
							public void onSuccess(String result) {
								Window.alert(result);
								election.onModuleLoad();
								
							} 	
						};
						
				service.registrazione(nomeTextBox.getValue(), 
						cognomeTextBox.getValue(), usernameTextBox.getValue(), passwordTextBox.getValue(), 
						telefonoTextBox.getValue(), 
						mailTextBox.getValue(), cfTextBox.getValue() , indirizzoTextBox.getValue(), 
						numDocTextBox.getValue(), callback);
				}
			}
		});

		RootPanel.add(registrazioneButton);
		RootPanel.setWidgetPosition(registrazioneButton, 630, 1100);
		final Button indietroButton = new Button("Indietro");
		indietroButton.setStyleName("buttonEsci");
		indietroButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				election.onModuleLoad();		
			}		
		});
		RootPanel.add(indietroButton);
		RootPanel.setWidgetPosition(indietroButton, 400, 210);
	}
}