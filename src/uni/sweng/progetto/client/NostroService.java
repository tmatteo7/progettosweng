package uni.sweng.progetto.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import uni.sweng.progetto.shared.Elezione;


@RemoteServiceRelativePath("service")
public interface NostroService extends RemoteService {
	
	
	//metodi relativi alla registrazione
	String registrazione(String nome, String cognome, String username, String password, String telefono,
	String email, String codiceFiscale, String indirizzoDomicilio, String idDocumentoIdentita) throws IllegalArgumentException;

	String nominaFunzionario(String username) throws IllegalArgumentException;
	
	//metodi relativi al login
	void loginCittadino(String username, String password) throws IllegalArgumentException;
	void loginFunzionario(String username, String password) throws IllegalArgumentException;
	void loginAdmin(String username, String password) throws IllegalArgumentException;
	
	//metodi relativi alla creazioni delle elezioni	
	void creaElezioni(Elezione e);
	ArrayList<Elezione> mostraElezioni();



}
