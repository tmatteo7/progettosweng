package uni.sweng.progetto.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Voto implements IsSerializable, Serializable{
	
	private String nomeLista;
	private String nomeElettore;
	private String preferenza;
	private Integer idElezione; 
	
	public Voto() {}
	
	public Voto(Integer idElezione,String nomeLista, String nomeElettore) {
		this.idElezione=idElezione;
		this.nomeLista = nomeLista;
		this.nomeElettore = nomeElettore;
		this.preferenza = null;
	}
	
//	public Voto(String nomeLista, String nomeElettore) {
//		
//		this.nomeLista = nomeLista;
//		this.nomeElettore = nomeElettore;
//		this.preferenza = null;
//	}
	
	public Voto(Integer idElezione, String nomeLista, String nomeElettore, String preferenza) {
		this.idElezione = idElezione;
		this.nomeLista = nomeLista;
		this.nomeElettore = nomeElettore;
		this.preferenza = preferenza;
	}
	
//	public Voto(String nomeLista, String nomeElettore, String preferenza) {
//		this.nomeLista = nomeLista;
//		this.nomeElettore = nomeElettore;
//		this.preferenza = preferenza;
//	}
	
	public Integer getIdElezione() {
		return this.idElezione;
	}

	public String getNomeLista() {
		return this.nomeLista;
	}

	public String getPreferenza() {
		return this.preferenza;
	}
	
	public String getElettore() {
		return this.nomeElettore;
	}

}
