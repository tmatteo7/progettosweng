package uni.sweng.progetto.shared;

import com.google.gwt.i18n.shared.CustomDateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.Date;
import java.util.Random;
import java.io.Serializable;

public class Elezione implements IsSerializable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String start;
	private String end;
	private int idElection;
	
	public Elezione(String start, String end) {
		Random random = new Random();
		int abc = random.nextInt(10000);
		this.idElection = abc;
		this.start = start;
		this.end = end;
	}
	
	public Elezione() {
	
	}
	
	
	public String getStringStart() {
		return start;
	}
	
	public String getStringEnd() {
		return end;
	}
	
	public int getIDElection() {
		return this.idElection;
	}
}
